const { build, Platform } = require('electron-builder')

function getCurrentPlatform() {
  switch (process.platform) {
    case 'win32':
      return Platform.WINDOWS
    case 'darwin':
      return Platform.MAC
    case 'linux':
      return Platform.LINUX
    default:
      console.error('Cannot resolve current platform!')
      return undefined
  }
}

build({
  targets: (process.argv[2] != null && Platform[process.argv[2]] != null ? Platform[process.argv[2]] : getCurrentPlatform()).createTarget(),
  config: {
    appId: 'fr.endrium.launcher',
    productName: 'Launcher Endrium',
    copyright: 'Copyright © 2020 Kasai.',
    artifactName: '${name}-${version}-${arch}.${ext}',
    generateUpdatesFilesForAllChannels: true,
    remoteBuild: true,
    removePackageScripts: true,
    directories: {
      buildResources: 'build',
      output: 'releases/${os}'
    },
    publish: [{
      provider: 'generic',
      url: 'https://alexm.best/endrium/launcher/releases/${os}'
    }],
    mac: {
      category: 'public.app-category.games',
      target: 'dmg',
      icon: 'build/icon.icns',
      darkModeSupport: true
    },
    dmg: {
      background: 'build/background.png',
      icon: 'build/icon.icns',
      internetEnabled: true
    },
    win: {
      target: [{
        target: 'nsis',
        arch: [
          'x64',
          'ia32'
        ]
      }],
      icon: 'build/icon.ico',
      legalTrademarks: 'Copyright © 2020 Kasai.',
      publisherName: 'Kasai.'
    },
    nsis: {
      oneClick: false,
      perMachine: true,
      allowElevation: true,
      allowToChangeInstallationDirectory: true,
      // installerIcon: 'build/win/installerIcon.ico',
      // uninstallerIcon: 'build/win/uninstallerIcon.ico',
      // installerHeader: 'build/win/installerHeader.bmp',
      // installerHeaderIcon: 'build/win/installerHeaderIcon.ico',
      // installerSidebar: 'build/win/installerSidebar.bmp',
      // uninstallerSidebar: 'build/win/uninstallerSidebar.bmp',
      deleteAppDataOnUninstall: true
    },
    linux: {
      target: [{
        target: 'AppImage',
        arch: [
          'x64',
          'ia32'
        ]
      }],
      icon: 'build/1024x1024.png',
      synopsis: 'Serveur moddé • Survie/SemiRP',
      description: 'Launcher personnalisé qui permet aux utilisateurs de rejoindre Endrium (Serveur moddé). Tous les mods, configurations et mises à jour sont gérés automatiquement.',
      category: 'Game'
    },
    compression: 'maximum',
    files: [
      '!{.vscode,releases,.editorconfig,.eslintrc.js,.gitignore,.gitlab-ci.yml,build.js}'
    ],
    extraResources: [
      'libraries'
    ],
    asar: true
  }
}).then(() => {
  console.log('Build complete!')
}).catch(err => {
  console.error('Error during build!', err)
})
