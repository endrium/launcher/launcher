const { init, addBreadcrumb, configureScope, captureMessage, captureException, captureEvent, showReportDialog } = require('@sentry/electron')

module.exports = {
  init,
  addBreadcrumb,
  configureScope,
  captureMessage,
  captureException,
  captureEvent,
  showReportDialog
}
