/* eslint-disable no-control-regex */
const net = require('net')

/**
 * Retrieves the status of a minecraft server.
 *
 * @param {string} address The server address.
 * @param {number} port Optional. The port of the server. Defaults to 25565.
 * @returns {Promise.<Object>} A promise which resolves to an object containing status information.
 */
exports.getStatus = (address, port = 25565) => {
  if (port == null || port === '') {
    port = 25565
  }

  if (typeof port === 'string') {
    port = parseInt(port)
  }

  return new Promise((resolve, reject) => {
    const socket = net.connect(port, address, () => {
      const buff = Buffer.from([0xFE, 0x01])
      socket.write(buff)
    })

    socket.setTimeout(2500, () => {
      socket.end()

      // eslint-disable-next-line prefer-promise-reject-errors
      reject({
        code: 'ETIMEDOUT',
        errno: 'ETIMEDOUT',
        address,
        port
      })
    })

    socket.on('data', data => {
      if (data != null && data !== '') {
        const serverInfos = data.toString().split('\x00\x00\x00')
        const NUM_FIELDS = 6

        if (serverInfos != null && serverInfos.length >= NUM_FIELDS) {
          resolve({
            online: true,
            version: serverInfos[2].replace(/\u0000/g, ''),
            motd: serverInfos[3].replace(/\u0000/g, ''),
            onlinePlayers: serverInfos[4].replace(/\u0000/g, ''),
            maxPlayers: serverInfos[5].replace(/\u0000/g, '')
          })
        } else {
          resolve({
            online: false
          })
        }
      }

      socket.end()
    })

    socket.on('error', err => {
      socket.destroy()
      reject(err)
      // ENOTFOUND = Unable to resolve.
      // ECONNREFUSED = Unable to connect to port.
    })
  })
}
