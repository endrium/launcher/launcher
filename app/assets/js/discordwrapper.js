// Work in progress
const logger = require('./loggerutil')('%c[DiscordWrapper]', 'color: #7289da; font-weight: bold')

const { Client } = require('discord-rpc')

let client
let activity

exports.initRPC = async(genSettings, servSettings, initialDetails) => {
  client = new Client({ transport: 'ipc' })

  activity = {
    state: servSettings.shortId,
    details: initialDetails,
    startTimestamp: new Date().getTime(),
    largeImageKey: servSettings.largeImageKey,
    largeImageText: servSettings.largeImageText,
    smallImageKey: genSettings.smallImageKey,
    smallImageText: genSettings.smallImageText,
    instance: false
  }

  client.setActivity()

  client.on('ready', async() => {
    logger.log('Discord RPC Connected')

    await client.setActivity(activity)
  })

  try {
    await client.login({ clientId: genSettings.clientId })
  } catch (error) {
    if (error.message.includes('ENOENT')) {
      logger.log('Unable to initialize Discord Rich Presence, no client detected.')
    } else {
      logger.log('Unable to initialize Discord Rich Presence: ' + error.message, error)
    }
  }
}

exports.updateDetails = async details => {
  activity.details = details

  await client.setActivity(activity)
}

exports.shutdownRPC = async() => {
  if (!client) return

  await client.clearActivity()
  await client.destroy()

  client = null
  activity = null
}
