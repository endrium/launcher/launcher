// Requirements
const { app, BrowserWindow, ipcMain, Menu } = require('electron')
const { autoUpdater } = require('electron-updater')
const { data: ejseData } = require('ejs-electron')
const { readdirSync } = require('fs')
const { join } = require('path')
const { format } = require('url')
const { prerelease } = require('semver')

const isDev = require('./app/assets/js/isdev')
const { init, captureException } = require('./app/assets/js/sentry')

init({
  dsn: 'https://a9e6b4b997cf4a199437f31a864fb49f@o389811.ingest.sentry.io/5228787',
  release: `${require('./package.json').name}@${require('./package.json').version}`,
  attachStacktrace: true,
  frameContextLines: 99999999999,
  maxBreadcrumbs: 100,
  maxValueLength: 99999999999
})

// Setup auto updater.
function initAutoUpdater(event, data) {
  if (data) autoUpdater.allowPrerelease = false
  else {
    // Defaults to true if application version contains prerelease components (e.g. 0.12.1-alpha.1)
    // autoUpdater.allowPrerelease = true
  }

  if (isDev) {
    autoUpdater.autoInstallOnAppQuit = false
    // autoUpdater.updateConfigPath = join(__dirname, 'dev-app-update.yml')
  }

  if (process.platform === 'darwin') autoUpdater.autoDownload = false

  autoUpdater.on('update-available', info => event.sender.send('autoUpdateNotification', 'update-available', info))
  autoUpdater.on('update-downloaded', info => event.sender.send('autoUpdateNotification', 'update-downloaded', info))
  autoUpdater.on('update-not-available', info => event.sender.send('autoUpdateNotification', 'update-not-available', info))
  autoUpdater.on('checking-for-update', () => event.sender.send('autoUpdateNotification', 'checking-for-update'))
  autoUpdater.on('error', err => event.sender.send('autoUpdateNotification', 'realerror', err))
}

// Open channel to listen for update actions.
ipcMain.on('autoUpdateAction', (event, arg, data) => {
  switch (arg) {
    case 'initAutoUpdater':
      console.log('Initializing auto updater.')
      initAutoUpdater(event, data)
      event.sender.send('autoUpdateNotification', 'ready')
      break

    case 'checkForUpdate':
      autoUpdater.checkForUpdates()
        .catch(err => {
          captureException(err)
          event.sender.send('autoUpdateNotification', 'realerror', err)
        })
      break

    case 'allowPrereleaseChange':
      if (!data) {
        const preRelComp = prerelease(app.getVersion())

        if (preRelComp != null && preRelComp.length > 0) autoUpdater.allowPrerelease = true
        else autoUpdater.allowPrerelease = data
      } else autoUpdater.allowPrerelease = data
      break

    case 'installUpdateNow':
      autoUpdater.quitAndInstall()
      break

    default:
      console.log('Unknown argument', arg)
      break
  }
})

// Redirect distribution index event from preloader to renderer.
ipcMain.on('distributionIndexDone', (event, res) => {
  event.sender.send('distributionIndexDone', res)
})

// Disable hardware acceleration.
// https://electronjs.org/docs/tutorial/offscreen-rendering
app.disableHardwareAcceleration()

// https://github.com/electron/electron/issues/18397
app.allowRendererProcessReuse = true

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win

function createWindow() {
  win = new BrowserWindow({
    width: 1220,
    height: 700,
    center: true,
    minWidth: 1220,
    minHeight: 700,
    maxWidth: 1920,
    maxHeight: 1080,
    icon: getPlatformIcon('SealCircle'),
    frame: false,
    backgroundColor: '#171614',
    darkTheme: true,
    webPreferences: {
      preload: join(__dirname, 'app', 'assets', 'js', 'preloader.js'),
      nodeIntegration: true,
      contextIsolation: false
    }
  })

  ejseData('bkid', Math.floor((Math.random() * readdirSync(join(__dirname, 'app', 'assets', 'img', 'bg')).length)))

  win.loadURL(format({
    pathname: join(__dirname, 'app', 'app.ejs'),
    protocol: 'file:',
    slashes: true
  }))

  win.removeMenu()
  win.resizable = true

  win.on('closed', () => {
    win = null
  })
}

function createMenu() {
  if (process.platform === 'darwin') {
    // Extend default included application menu to continue support for quit keyboard shortcut
    const applicationSubMenu = {
      label: 'Application',
      submenu: [{
        label: 'Quitter',
        accelerator: 'Command+Q',
        click: () => app.quit()
      }]
    }

    // Bundle submenus into a single template and build a menu object with it
    const menuObject = Menu.buildFromTemplate(applicationSubMenu)

    // Assign it to the application
    Menu.setApplicationMenu(menuObject)
  }
}

function getPlatformIcon(filename) {
  const opSys = process.platform

  if (opSys === 'darwin') {
    filename = filename + '.icns'
  } else if (opSys === 'win32') {
    filename = filename + '.ico'
  } else {
    filename = filename + '.png'
  }

  return join(__dirname, 'app', 'assets', 'img', filename)
}

app.on('ready', createWindow)
app.on('ready', createMenu)

app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow()
  }
})
